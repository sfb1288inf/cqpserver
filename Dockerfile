FROM debian:buster-slim


LABEL authors="Patrick Jentsch <p.jentsch@uni-bielefeld.de>"


ARG CWB_REVISION=1674
ARG CWB_PLATFORM=linux-64
ARG CWB_SITE=standard
ENV LANG=C.UTF-8


EXPOSE 4877


WORKDIR /root


## Install IMS Open Corpus Workbench ##
RUN apt-get update \
 && apt-get install --no-install-recommends --yes \
    subversion autoconf bison flex gcc make pkg-config libc6-dev libncurses5 libncurses5-dev libpcre3-dev libglib2.0-0 libglib2.0-dev libreadline7 libreadline-dev \
 && svn checkout "http://svn.code.sf.net/p/cwb/code/cwb/trunk@${CWB_REVISION}" cwb \
 && cd cwb \
 && make clean PLATFORM=${CWB_PLATFORM} SITE=${CWB_SITE} \
 && make depend PLATFORM=${CWB_PLATFORM} SITE=${CWB_SITE} \
 && make all PLATFORM=${CWB_PLATFORM} SITE=${CWB_SITE} \
 && make install PLATFORM=${CWB_PLATFORM} SITE=${CWB_SITE} \
 && make realclean PLATFORM=${CWB_PLATFORM} SITE=${CWB_SITE} \
 && cd - > /dev/null \
 && rm -r cwb


## Install Perl CWB package ##
RUN yes | cpan HTML::Entities \
 && svn checkout "http://svn.code.sf.net/p/cwb/code/perl/trunk/CWB@${CWB_REVISION}" perl-cwb \
 && cd perl-cwb \
 && perl Makefile.PL \
 && make \
 && make test \
 && make install \
 && cd - > /dev/null \
 && rm -r perl-cwb


COPY docker-entrypoint.sh /usr/local/bin/


RUN rm -r /var/lib/apt/lists/*


ENTRYPOINT ["docker-entrypoint.sh"]
