#!/bin/bash
if [[ "$#" -eq 0 || "${1}" == "cqpserver" ]]; then
  echo "host ${CQPSERVER_HOST:-*};" > cqpserver.init
  echo "user ${CQPSERVER_USER:-anonymous} \"${CQPSERVER_PASSWORD:-}\";" >> cqpserver.init
fi

if [[ "$#" -eq 0 ]]; then
  cqpserver -I cqpserver.init
elif [[ "$1" == "build-corpus" ]]; then
  # Input:
  #   - Source file: "/root/files/corpus.vrt"
  # Output
  #   - Corpus name: "CORPUS"
  #   - Corpus data directory: "/corpora/data/corpus"
  #   - Corpus registry file: "/usr/local/share/cwb/registry/corpus"
  mkdir -p /corpora/data/corpus
  cwb-encode \
    -c utf8 \
    -d /corpora/data/corpus \
    -f /root/files/corpus.vrt \
    -R /usr/local/share/cwb/registry/corpus \
    -P pos -P lemma -P simple_pos -P ner \
    -S ent:0+type -S s \
    -S text:0+address+author+booktitle+chapter+editor+institution+journal+pages+publisher+publishing_year+school+title \
    -xsB -9
  cwb-make \
    -V CORPUS
elif [ "${1}" == "cqpserver" ]; then
  cqpserver -I cqpserver.init "${@:2}"
else
  exec ${@:1}
    echo "$0 [COMMAND]"
    echo ""
    echo "cqpserver startup and corpus build script"
    echo ""
    echo "Commands:"
    echo "  build-corpus"
    echo "  cqpserver"
fi
